angular.module('supportalApp')
  .constant("config", {
    checkAccountStatusInterval: 10000, // interval between each account availability check
    cookieExpirationDate: new Date("December 31, 2100 06:00:00"), //default expiration date for cookies. This defines how long the history and other settings are saved on the computer
    salesforceAPIDomain: { // these are the domains which are used to check for hotnews and account information from salesforce
      hotNews: "http://prodkb.tlv.lpnet.com:3001/hotnewsjson?site=",
      accountInfo: "http://prodkb.tlv.lpnet.com:3001/account?site="
    },
    maxUnknownRechecks: 5, //how many times to check availability if 'unknown' was returned. After this, we stop checking availability and assume it is unknown
    defaultServerUrl: "server.iad.liveperson.net", //default url which appears in the server domain dropdown
    serverDomains: [   // these are the server domains which are available in the server domain dropdown
      {
        name: "LP",
        url: "server.iad.liveperson.net"
      },
      {
        name: "SALES",
        url: "sales.liveperson.net"
      },
      {
        name: "UK",
        url: "server.lon.liveperson.net"
      },
      {
        name: "APAC",
        url: "server.sys.liveperson.net"
      },
      {
        name: "BANA",
        url: "sec1.liveperson.net"
      },
      {
        name: "BASE",
        url: "base.liveperson.net"
      },
      {
        name: "DEV",
        url: "dev.liveperson.net"
      },
      {
        name: "HC",
        url: "hc2.humanclick.com"
      },
      {
        name: "HDWEB1",
        url: "hdweb1"
      },
      {
        name: "HC1DEV",
        url: "hc1.dev.lprnd.net"
      },
      {
        name: "LPWEBDEMO",
        url: "lpwebdemo.liveperson.net"
      },
      {
        name: "DEV-WIN",
        url: "dev-win"
      }
    ],
    //these are the functions that will appears in the tools area of the Supportal.
    // The 'groupName' defines which group it will be assigned to and the 'functions' is an array of all the functions in the group
    groupedFunctions: [
      {
        "groupName": "Frequently Used",
        "functions": [
          {
            name: "Provisions Info", //eg: name to display on button
            url: "/hc/web/siteadmin/provisionInfo.jsp?site=$account", //eg: url to invoke. the $xxxxx indicates a required field which is populated from the forms. Available options are: $account, $livepersonPassword, $livepersonUsername
            icon: "fa fa-list", //eg: icon to display on the button
            description: "Open Provisions Info", //eg: description of the shortcut to display on the shortcut menu
            shortcut: "ctrl+shift+p" //eg: shortcut key combination which invokes this function
          },
          {
            name: "Admin Console",
            url: "/hc/web/mlogin/home.jsp?user=$livepersonUsername&pass=$livepersonPassword&site=$account",
            icon: "fa fa-cog",
            description: "Open Admin Console",
            shortcut: "ctrl+shift+a"
          },
          {
            name: "LiveEngage Admin",
            url: "/hc/web/mlogin/home.jsp?user=$livepersonUsername&pass=$livepersonPassword&site=$account&redirectToLiveEngage=false&ac=true",
            icon: "fa fa-list",
            description: "Open LiveEngage Admin",
            shortcut: "ctrl+shift+l"
          },
          {
            name: "LE Web Agent",
            url: "/hc/web/mlogin/login.jsp?user=$livepersonUsername&pass=$livepersonPassword&site=$account&lpservice=liveEngage",
            icon: "fa fa-list",
            description: "Access LiveEngage's Web Agent",
            shortcut: "ctrl+shift+w"
          },
          {
            name: "LPAdmin JSP",
            url: "/hc/web/siteadmin/LPAdminAccess.jsp?site=$account",
            icon: "fa fa-list",
            description: "Enable LPA Admin",
            shortcut: "ctrl++shift+j"
          }
        ]
      },
      {
        "groupName": "Account Control",
        "functions": [
          {
            name: "Features",
            url: "/hc/web/siteadmin/features.jsp?site=$account",
            icon: "fa fa-list"
          },
          {
            name: "Provision",
            url: "/hc/web/siteadmin/provisionInfo.jsp?site=$account",
            icon: "fa fa-list"
          },
          {
            name: "Voice Provision",
            url: "/hc/web/siteadmin/provisionvoice.jsp?site=$account",
            icon: "glyphicon glyphicon-earphone"
          },
          {
            name: "Account Config",
            url: "/hc/web/siteadmin/accountConfigFeatures.jsp?site=$account",
            icon: "fa fa-list"
          },
          {
            name: "Redirect Site",
            url: "url",
            icon: "glyphicon glyphicon-transfer"
          },
          {
            name: "Reset Password",
            url: "/hc/web/public/pub/ma/lp/forgotMyPassword.jsp?user=$livepersonUsername&site=$account",
            icon: "fa fa-lock",
            description: "Reset Password",
            shortcut: "ctrl+shift+r"
          },
          {
            name: "Site Gen Props",
            url: "/hc/web/siteadmin/sitegenprops.jsp?site=$account",
            icon: "fa fa-list"
          },
          {
            name: "Add Packages",
            url: "/hc/web/siteadmin/packages.jsp?site=$account",
            icon: "icon icon-present"
          }
        ]
      },
      {
        "groupName": "Account Info",
        "functions": [
          {
            name: "Features Info",
            url: "/hc/web/mlogin/login.jsp",
            icon: "fa fa-list"
          },
          {
            name: "Provisions Info",
            url: "/hc/web/siteadmin/provisionInfo.jsp?site=$account",
            icon: "fa fa-list"
          },
          {
            name: "Voice Info",
            url: "/hc/web/siteadmin/provisionvoiceInfo.jsp?site=$account",
            icon: "icon icon-call-out"
          },
          {
            name: "Service Info",
            url: "/hc/web/siteadmin/serviceInfo.jsp?site=$account",
            icon: "fa fa-list",
            groups: [
              1
            ]
          },
          {
            name: "Who Has Feature",
            url: "/hc/web/admin/admin/whoHasTheFeature.jsp?site=$account",
            icon: "fa fa-list"
          }
        ]
      },
      {
        "groupName": "Server/DB",
        "functions": [
          {
            name: "SQL",
            url: "/hc/web/admin/admin/SQL.jsp",
            icon: "fa fa-list"
          },
          {
            name: "RSI",
            url: "/hc/web/siteadmin/rsi.jsp?site=$account",
            icon: "fa fa-list"

          },
          {
            name: "Server Versions",
            url: "http://hc1d/serversVersion.jsp",
            icon: "fa fa-globe"

          },
          {
            name: "RIFS",
            url: "http://tlvsales/production/RIFS.html",
            icon: "fa fa-list"
          }
        ]
      },
      {
        "groupName": "Ticketing",
        "functions": [
          {
            name: "Reset Counters",
            url: "/hc/web/admin/patchSites.jsp?site=$account&cmd=reloadTicketCounters",
            icon: "icon icon-ban"
          },
          {
            name: "Multiple Ticket",
            url: "/hc/web/mlogin/tkt/MultipleOpControl.jsp?site=$account&user=$livepersonUsername&pass=$livepersonPassword",
            icon: "fa fa-ticket"

          },
          {
            name: "Ticket History",
            url: "/hc/web/mlogin/tkt/tkthistory.jsp?site=$account&user=$livepersonUsername&pass=$livepersonPassword&type=allTicket&tid=$ticket",
            icon: "fa fa-history"
          },
          {
            name: "Ticket Buster",
            url: "/hc/web/mlogin/tkt/oneticket.jsp?site=$account&user=$livepersonUsername&pass=$livepersonPassword&type=allTicket&tid=$ticket",
            icon: "fa fa-history"
          }
        ]
      },
      {
        "groupName": "Visitor Tools",
        "functions": [
          {
            name: "Create Visitor (Legacy)",
            url: "http://hc1d/createSite.jsp?siteid=$account&server=sales.liveperson.net",
            icon: "icon icon-user-follow"
          },
          {
            name: "Create Visitor α",
            url: "http://hc1d/smtprod1.jsp?siteid=$account",
            icon: "icon icon-user-follow"
          },
          {
            name: "Create Visitor GA",
            url: "http://hc1d/smtprodga1.jsp?siteid=$account",
            icon: "icon icon-user-follow"
          },
          {
            name: "Visitor Logout",
            url: "/hc/5296924/?cmd=visitorLogout&site=$account",
            icon: "icon icon-logout"
          }
        ]
      },
      {
        "groupName": "API Tools",
        "functions": [
          {
            name: "API Keys Admin",
            url: "url",
            icon: "icon icon-key"
          },
          {
            name: "Chat API Test",
            url: "http://tlvsales/alert/chatapi.asp",
            icon: "icon icon-speech"
          },
          {
            name: "AppKey Check",
            url: "/hc/web/admin/admin/appKeysAdmin.jsp?server=svpp-ajv01&cmd=viewOne&idToView=$appKey",
            icon: "fa fa-history"
          }
        ]
      },
      {
        "groupName": "Less Used",
        "functions": [
          {
            name: "Star Rating",
            url: "/hc/web/siteadmin/starRating.jsp?site=$account",
            icon: "fa fa-star"
          },
          {
            name: "KB Manage",
            url: "/hc/web/siteadmin/kbmanage.jsp?site=$account",
            icon: "icon icon-book-open"
          },
          {
            name: "Register No Credit",
            url: "/hc/web/public/pub/adminlogin.jsp?goto=admin/registerNoCC.jsp",
            icon: "fa fa-credit-card"
          },
          {
            name: "Features List",
            url: "/hc/web/admin/admin/featuresList.jsp",
            icon: "icon icon-list"
          }
        ]
      }
    ],
    maxHistorySize: 20, // how many accounts should the history remember before overwriting
    testAccounts: [ //list of the test accounts
      {
        account: "B23069388",
        accountType: "LivePerson Express",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B57415959",
        accountType: "LivePerson Pro",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B59423048",
        accountType: "LivePerson Contact Center Standard",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B63456855",
        accountType: "LivePerson Contact Center",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B70660793",
        accountType: "LivePerson Pro Plus",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B55626153",
        accountType: "LivePerson Contact Center Standard",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B45828789",
        accountType: "Timpani Contact Center Plus",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "B15184190",
        accountType: "Timpani Chat",
        serverDomain: "server.iad.liveperson.net",
        server: "scal2",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      },
      {
        account: "42111609",
        accountType: "Timpani Sales & Marketing",
        serverDomain: "server.iad.liveperson.net",
        server: "ssal13",
        username: "admin",
        password: "1",
        expiration: "08/08/08"
      }
    ],
    lpaAccounts: [ //list of LPA accounts
      {
        account: "LPadministration",
        accountType: "Enterprise",
        serverUrl: "sales.liveperson.net"
      },
      {
        account: "56967968",
        accountType: "SMB",
        serverUrl: "server.iad.liveperson.net"
      },
      {
        account: "P13701350",
        accountType: "Partners",
        serverUrl: "dev.liveperson.net"
      },
      {
        account: "7503245",
        accountType: "BANA",
        serverUrl: "sec1.liveperson.net"
      },
      {
        account: "28307345",
        accountType: "UK",
        serverUrl: "server.lon.liveperson.net"
      },
      {
        account: "18719417",
        accountType: "APAC",
        serverUrl: "server.sys.liveperson.net"
      }
    ],
    avatars: ["Alien-icon.png", "Angry-Man-icon.png", "Bat-icon.png", "Casper-icon.png", "Chucky-icon.png", "Clown-2-icon.png", "Clown-icon.png", "Devil-2-icon.png", "Devil-3-icon.png", "Devil-icon.png", "Doll-icon.png", "Dracula-icon.png", "Face-Paint-icon.png", "Frankenstein-icon.png", "Freddie-icon.png", "Ghost-icon.png", "Ghoul-icon.png", "Gomez-icon.png", "Grim-Reaper-icon.png", "Jack-Skellington-icon.png", "Jason-icon.png", "Kokey-icon.png", "Mask-3-icon.png", "Mask-4-icon.png", "Mask-5-icon.png", "Mike-icon.png", "Mummy-icon.png", "Pumpkin-icon.png", "Red-Skull-icon.png", "Scream-icon.png", "Sea-Monster-icon.png", "Skull-icon.png", "Slasher-2-icon.png", "Slasher-icon.png", "Slimer-icon.png", "Voodoo-Doll-icon.png", "Werewolf-icon.png", "Witch-icon.png", "Zombie-2-icon.png", "Zombie-Female-icon.png", "Zombie-icon.png"], //list of avatar images
    customImages: ["reponline.gif", "repoffline.gif", "repoccupied.gif", "voice_reponline.gif", "voice_repoffline.gif", "voice_repoccupied.gif", "need_help_on.gif", "need_help_off.gif", "close_on.gif", "close_off.gif", "voice_need_help_on.gif", "voice_need_help_off.gif", "voice_close_on.gif", "voice_close_off.gif", "oneStepVoiceButtonDefault.gif", "oneStepVoiceButtonRepOccupied.gif", "oneStepVoiceButtonRepOffline.gif", "oneStepVoiceButtonRepOnline.gif", "oneStepVoiceDefault.gif", "oneStepVoiceInvitationBKG.gif", "oneStepVoiceInvitationButton.gif", "oneStepVoiceInvitationClose.gif", "oneStepVoiceRepOccupied.gif", "oneStepVoiceRepOffline.gif", "oneStepVoiceRepOnline.gif", "transparent.gif", "voice_default.gif"] //deprecated: list of images for button availability
  }
)
  //tooltips and captions for I18N readiness
  .constant("captions", {
    http: "HTTP",
    https: "HTTPS",
    fileName: "File Name",
    more: "More",
    less: "Less",
    save: "Save",
    saved: "Saved",
    reset: "Reset",
    login: "Login",
    settings: "Settings",
    supportal: "Supportal",
    functions: "Functions",
    noHotNews: "No Hot News Found",
    noAccountInfo: "No Account Info Found",
    supportTools: "Support Tools",
    accountInfo: "Account Information",
    serverInfo: "Server Information",
    accountHotNews: "Account Hot News",
    resetPassword: "Reset Password",
    adminConsole: "Admin Console",
    emptyHistory: "Looks like there are no historical accounts to display. History will appear as you access account functions.",
    emptyFavorites: "Looks like there are no favorite accounts to display. You can add them in 'settings'",
    emptyActivity: "Looks like there is no recent activity to display. Get cracking!",
    copiedToClipboard: "Successfully copied to clipboard",
    certificateError: "HTTPS certificate error. Open this URL in a new window and approve this domain to get Salesforce information: ",
    emailAddress: "Email Address",
    openLinksInNewWindow: "Open Links in New Window",
    alwaysShowExpandedTools: "Always Show Expanded Tools",
    alwaysShowAccountInformation: "Show Account Information by Default",
    recentAccounts: "Recent Accounts",
    recentActivity: "Recent Activity",
    savedCredentials: "Saved Credentials",
    preferences: "Preferences",
    favoriteAccounts: "Favorite Accounts",
    lpaAccounts: "LPA Accounts",
    defaultServerFarm: "Default Server Farm",
    customImages: "Custom Images",
    profileSettings: "Profile Settings",
    filterImages: "Filter Images",
    filterFunctions: "Filter Functions",
    account: "Account Number",
    accountType: "Account Type",
    server: "Server",
    username: "Username",
    password: "Password",
    expires: "Expires",
    serverDomain: "Server Domain",
    missingParameters: "You have some missing parameters to call this function."
  }
)
  .constant("constants", {  //states used in logic throughout the supportal functions
    LP_CHAT_STATES: {
      UNKNOWN: "Unknown",
      OFFLINE: "Offline",
      ONLINE: "Online",
      OCCUPIED: "Back in 5",
      CHECKING: "Checking"
    }
  }
);
