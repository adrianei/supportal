'use strict';

/**
 * @ngdoc function
 * @name supportalApp.controller:ClockCtrl
 * @description
 * # ClockCtrl
 * Controller of the supportalApp.
 * This code was extracted from the following source: http://www.proglogic.com/code/javascript/time/worldclock.php
 */
angular.module('supportalApp')
  .controller('ClockCtrl', function ($scope, $interval) {

    $scope.countries = [
      //{ name: "ADD_COUNTRY_NAME", selector: "GMT", offset: 0, location: "Greenwich"},
      {name: "San Francisco, CA", selector: "SanFrancisco", offset: -8, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Vancouver", offset: -8, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Seattle", offset: -8, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "LosAngeles", offset: -8, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Denver", offset: -7, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "MexicoCity", offset: -6, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Houston", offset: -6, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Minneapolis", offset: -6, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "NewOrleans", offset: -6, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Chicago", offset: -6, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Montgomery", offset: -6, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Indianapolis", offset: -5, location: "NAmerica"},
      {name: "Atlanta, GA", selector: "Atlanta", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Detroit", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Miami", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "WashingtonDC", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Philadelphia", offset: -5, location: "NAmerica"},
      {name: "New York, NY", selector: "NewYork", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Montreal", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Boston", offset: -5, location: "NAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "BuenosAires", offset: -3, location: "BuenosAires"},
      //{ name: "ADD_COUNTRY_NAME", selector: "SaoPaulo", offset: -3, location: "SAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "RioDeJaneiro", offset: -3, location: "SAmerica"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Lisbon", offset: 0, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Dublin", offset: 0, location: "Europe"},
      {name: "London, UK", selector: "London", offset: 0, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Madrid", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Barcelona", offset: 1, location: "Europe"},
      {name: "Paris, FR", selector: "Paris", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Brussels", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Amsterdam", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Frankfurt", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Rome", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Berlin", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Prague", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Vienna", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Stockholm", offset: 1, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Athens", offset: 2, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Helsinki", offset: 2, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Minsk", offset: 2, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Istanbul", offset: 2, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Cairo", offset: 2, location: "Cairo"},
      {name: "Tel Aviv, IL", selector: "Jerusalem", offset: 2, location: "Israel"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Beirut", offset: 2, location: "Beirut"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Moscow", offset: 3, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Baghdad", offset: 3, location: "Baghdad"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Dubai", offset: 4, location: "Dubai"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Bangkok", offset: 7, location: "Bangkok"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Jakarta", offset: 7, location: "Jakarta"},
      //{ name: "ADD_COUNTRY_NAME", selector: "HongKong", offset: 8, location: "HongKong"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Beijing", offset: 8, location: "Beijing"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Shanghai", offset: 8, location: "Shanghai"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Seoul", offset: 9, location: "Seoul"},
      { name: "Tokyo, JP", selector: "Tokyo", offset: 9, location: "Tokyo"},
      {name: "Melbourne, AU", selector: "Melbourne", offset: 10, location: "Australia"}
      //{ name: "ADD_COUNTRY_NAME", selector: "Sydney", offset: 10, location: "Australia"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Brisbane", offset: 10, location: "Brisbane"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Vladivostok", offset: 10, location: "Europe"},
      //{ name: "ADD_COUNTRY_NAME", selector: "Kamchatka", offset: 12, location: "Europe"}
    ];

    init();

    function worldClock(location, zone, region) {
      var dst = 0;
      var time = new Date();
      var gmtMS = time.getTime() + (time.getTimezoneOffset() * 60000);
      var gmtTime = new Date(gmtMS);
      var day = gmtTime.getDate();
      var month = gmtTime.getMonth();
      var year = gmtTime.getYear();
      if (year < 1000) {
        year += 1900
      }
      var monthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

      var monthDays = ["31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];
      if (year % 4 == 0) {
        monthDays = ["31", "29", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"]
      }
      if (year % 100 == 0 && year % 400 != 0) {
        monthDays = ["31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"]
      }

      var hr = gmtTime.getHours() + zone;
      var min = gmtTime.getMinutes();
      var sec = gmtTime.getSeconds();

      if (hr >= 24) {
        hr = hr - 24;
        day -= -1
      }
      if (hr < 0) {
        hr -= -24;
        day -= 1
      }
      if (hr < 10) {
        hr = " " + hr
      }
      if (min < 10) {
        min = "0" + min
      }
      if (sec < 10) {
        sec = "0" + sec
      }
      if (day <= 0) {
        if (month == 0) {
          month = 11;
          year -= 1
        }
        else {
          month -= 1
        }
        day = monthDays[month]
      }
      if (day > monthDays[month]) {
        day = 1;
        if (month == 11) {
          month = 0;
          year -= -1
        }
        else {
          month -= -1
        }
      }
      if (region == "NAmerica") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(3);
        startDST.setHours(2);
        startDST.setDate(1);
        var dayDST = startDST.getDay();
        if (dayDST != 0) {
          startDST.setDate(8 - dayDST)
        }
        else {
          startDST.setDate(1)
        }
        endDST.setMonth(9);
        endDST.setHours(1);
        endDST.setDate(31);
        dayDST = endDST.getDay();
        endDST.setDate(31 - dayDST);
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST && currentTime < endDST) {
          dst = 1
        }
      }
      if (region == "Europe") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(2);
        startDST.setHours(1);
        startDST.setDate(31);
        var dayDST = startDST.getDay();
        startDST.setDate(31 - dayDST);
        endDST.setMonth(9);
        endDST.setHours(0);
        endDST.setDate(31);
        dayDST = endDST.getDay();
        endDST.setDate(31 - dayDST);
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST && currentTime < endDST) {
          dst = 1
        }
      }

      if (region == "SAmerica") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(9);
        startDST.setHours(0);
        startDST.setDate(1);
        var dayDST = startDST.getDay();
        if (dayDST != 0) {
          startDST.setDate(22 - dayDST)
        }
        else {
          startDST.setDate(15)
        }
        endDST.setMonth(1);
        endDST.setHours(11);
        endDST.setDate(1);
        dayDST = endDST.getDay();
        if (dayDST != 0) {
          endDST.setDate(21 - dayDST)
        }
        else {
          endDST.setDate(14)
        }
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST || currentTime < endDST) {
          dst = 1
        }
      }
      if (region == "Cairo") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(3);
        startDST.setHours(0);
        startDST.setDate(30);
        var dayDST = startDST.getDay();
        if (dayDST < 5) {
          startDST.setDate(28 - dayDST)
        }
        else {
          startDST.setDate(35 - dayDST)
        }
        endDST.setMonth(8);
        endDST.setHours(11);
        endDST.setDate(30);
        dayDST = endDST.getDay();
        if (dayDST < 4) {
          endDST.setDate(27 - dayDST)
        }
        else {
          endDST.setDate(34 - dayDST)
        }
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST && currentTime < endDST) {
          dst = 1
        }
      }
      if (region == "Israel") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(3);
        startDST.setHours(2);
        startDST.setDate(1);
        endDST.setMonth(8);
        endDST.setHours(2);
        endDST.setDate(25);
        dayDST = endDST.getDay();
        if (dayDST != 0) {
          endDST.setDate(32 - dayDST)
        }
        else {
          endDST.setDate(1);
          endDST.setMonth(9)
        }
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST && currentTime < endDST) {
          dst = 1
        }
      }
      if (region == "Beirut") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(2);
        startDST.setHours(0);
        startDST.setDate(31);
        var dayDST = startDST.getDay();
        startDST.setDate(31 - dayDST);
        endDST.setMonth(9);
        endDST.setHours(11);
        endDST.setDate(31);
        dayDST = endDST.getDay();
        endDST.setDate(30 - dayDST);
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST && currentTime < endDST) {
          dst = 1
        }
      }
      if (region == "Baghdad") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(3);
        startDST.setHours(3);
        startDST.setDate(1);
        endDST.setMonth(9);
        endDST.setHours(3);
        endDST.setDate(1);
        dayDST = endDST.getDay();
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST && currentTime < endDST) {
          dst = 1
        }
      }
      if (region == "Australia") {
        var startDST = new Date();
        var endDST = new Date();
        startDST.setMonth(9);
        startDST.setHours(2);
        startDST.setDate(31);
        var dayDST = startDST.getDay();
        startDST.setDate(31 - dayDST);
        endDST.setMonth(2);
        endDST.setHours(2);
        endDST.setDate(31);
        dayDST = endDST.getDay();
        endDST.setDate(31 - dayDST);
        var currentTime = new Date();
        currentTime.setMonth(month);
        currentTime.setYear(year);
        currentTime.setDate(day);
        currentTime.setHours(hr);
        if (currentTime >= startDST || currentTime < endDST) {
          dst = 1
        }
      }


      function hour12(hr) {
        if (hr > 12) {
          return hr - 12;
        } else if (hr == 0) {
          return 12;
        } else {
          return hr;
        }
      }

      function ampm(hr) {
        if (hr >= 12 && hr <= 23) {
          return " PM";
        } else {
          return " AM";
        }
      }


      if (dst == 1) {
        hr -= -1;
        if (hr >= 24) {
          hr = hr - 24;
          day -= -1
        }
        if (hr < 10) {
          hr = "0" + hr
        }
        if (day > monthDays[month]) {
          day = 1;
          if (month == 11) {
            month = 0;
            year -= -1
          }
          else {
            month -= -1
          }
        }
        return "<span class='clock'><span class='name'>" + location + "</span><br /><small class='time'>" + hour12(hr) + ":" + min + ":" + sec + ampm(hr) + " DST" + "</small></span>";
      }
      else {
        var hourA = hour12(hr);
        return "<span class='clock'><span class='name'>" + location + "</span><br /><small class='time'>" + hour12(hr) + ":" + min + ":" + sec + ampm(hr) + "</small></span>";
      }
    }

    function worldClockZone() {
      _.forEach($scope.countries, function (country) {
        angular.element("#" + country.selector).html(worldClock(country.name, country.offset, country.location));
      });
    }

    function init() {
      var clockRefresh = $interval(worldClockZone, 1000);
    }
  });
