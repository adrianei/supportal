'use strict';

/**
 * @ngdoc function
 * @name supportalApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the supportalApp
 */
angular.module('supportalApp')
  .controller('SettingsCtrl', function ($scope, $rootScope, $cookies, cookieService, $parse, config, captions) {
    init();

    function init() {
      getCookies();
      $scope.changesDetected = false;
      $scope.config = config;
      $scope.captions = captions;
      $scope.defaultServerName = '[' + _.find(config.serverDomains, function (domain) {
        return domain.url === $scope.cookies.defaultServerUrl
      }).name + '] - ' + $scope.cookies.defaultServerUrl;
      $scope.defaultServerUrl = config.serverDomains[0].url;
    }

    $scope.$watchGroup(["cookies.personalFullName", "cookies.personalEmailAddress", "cookies.salesforceUsername", "cookies.salesforcePassword", "cookies.livepersonUsername", "cookies.livepersonPassword", "cookies.exactTargetUsername", "cookies.exactTargetPassword"], function (oldValue, newValue) {
      if (oldValue !== newValue) {
        $scope.changesDetected = true;
      }
    });

    $scope.$watchGroup(['cookies.linkWindowTarget', 'cookies.showAdvancedTools',  'cookies.defaultServerUrl'], setPreferenceCookies);

    $scope.$watch('cookies.favorites', function () {
      $scope.updateFavorites();
    }, true);

    function setPreferenceCookies() {
      $cookies.put("linkWindowTarget", $scope.cookies.linkWindowTarget, {'expires': config.cookieExpirationDate});
      $cookies.put("showAdvancedTools", $scope.cookies.showAdvancedTools, {'expires': config.cookieExpirationDate});
      $cookies.put("defaultServerUrl", $scope.cookies.defaultServerUrl, {'expires': config.cookieExpirationDate});
    }

    function getCookies() {
      $scope.cookies = cookieService.getCookies();
      //add a new cookie holder for the settings page
      $scope.cookies.favorites.push({
        account: null,
        serverUrl: null
      });
    }

    $scope.updateFavorites = function () {
      var favoritesWithoutBlanks = _.filter($scope.cookies.favorites, function (favorite) {
        return favorite.account !== "" && favorite.account !== null;
      });

      $cookies.put("favorites", JSON.stringify(favoritesWithoutBlanks), {'expires': config.cookieExpirationDate});
      if ($scope.cookies.favorites[$scope.cookies.favorites.length - 1].account !== null && $scope.cookies.favorites[$scope.cookies.favorites.length - 1].account !== "") {
        $scope.cookies.favorites.push({
          account: null,
          serverUrl: null
        });
      }
    };

    $scope.removeFavorite = function (index) {
      $scope.cookies.favorites.splice(index, 1);
      $cookies.put("favorites", JSON.stringify($scope.cookies.favorites), {'expires': config.cookieExpirationDate});
    };

    $scope.setCredentialCookies = function () {
      if ($scope.cookies.personalFullName) {
        $cookies.put("personalFullName", $scope.cookies.personalFullName, {'expires': config.cookieExpirationDate});
      } else {
        $cookies.remove("personalFullName");
      }

      if ($scope.cookies.personalEmailAddress) {
        $cookies.put("personalEmailAddress", $scope.cookies.personalEmailAddress, {'expires': config.cookieExpirationDate});
      } else {
        $cookies.remove("personalEmailAddress");
      }

      //if ($scope.cookies.salesforceUsername) {
      //  $cookies.put("salesforceUsername", $scope.cookies.salesforceUsername, {'expires': config.cookieExpirationDate});
      //} else {
      //  $cookies.remove("salesforceUsername");
      //}
      //
      //if ($scope.cookies.salesforcePassword) {
      //  $cookies.put("salesforcePassword", $scope.cookies.salesforcePassword, {'expires': config.cookieExpirationDate});
      //} else {
      //  $cookies.remove("salesforcePassword");
      //}
      //
      //if ($scope.cookies.livepersonUsername) {
      //  $cookies.put("livepersonUsername", $scope.cookies.livepersonUsername, {'expires': config.cookieExpirationDate});
      //} else {
      //  $cookies.remove("livepersonUsername");
      //}
      //
      //if ($scope.cookies.livepersonPassword) {
      //  $cookies.put("livepersonPassword", $scope.cookies.livepersonPassword, {'expires': config.cookieExpirationDate});
      //} else {
      //  $cookies.remove("livepersonPassword");
      //}

      if ($scope.cookies.exactTargetUsername) {
        $cookies.put("exactTargetUsername", $scope.cookies.exactTargetUsername, {'expires': config.cookieExpirationDate});
      } else {
        $cookies.remove("exactTargetUsername");
      }

      if ($scope.cookies.exactTargetPassword) {
        $cookies.put("exactTargetPassword", $scope.cookies.exactTargetPassword, {'expires': config.cookieExpirationDate});
      } else {
        $cookies.remove("exactTargetPassword");
      }
      $scope.changesDetected = false;
    };

    $scope.clearCredentialCookies = function () {
      $scope.cookies.personalFullName = null;
      $scope.cookies.personalEmailAddress = null;
      $scope.cookies.salesforceUsername = null;
      $scope.cookies.salesforcePassword = null;
      $scope.cookies.livepersonUsername = null;
      $scope.cookies.livepersonPassword = null;
      $scope.cookies.exactTargetUsername = null;
      $scope.cookies.exactTargetPassword = null;
    };

    $scope.setActiveDomain = function (url) {
      $scope.cookies.defaultServerUrl = url;
      $scope.defaultServerName = '[' + _.find(config.serverDomains, function (domain) {
        return domain.url === $scope.cookies.defaultServerUrl
      }).name + '] - ' + $scope.cookies.defaultServerUrl;
    };

    $scope.changeCurrentDomain = function (index, url) {
      $scope.cookies.favorites[index].url = url;
    };

    $scope.setAvatar = function (avatar) {
      $rootScope.userAvatar = avatar;
      $cookies.put("userAvatar", avatar, {'expires': config.cookieExpirationDate});
    };
  });
