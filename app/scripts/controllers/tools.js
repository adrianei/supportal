'use strict';

/**
 * @ngdoc function
 * @name supportalApp.controller:SettingsCtrl
 * @description
 * # SettingsCtrl
 * Controller of the supportalApp
 */
angular.module('supportalApp')
  .controller('ToolsCtrl', function ($scope, $rootScope, $cookies, cookieService, $sce, $q, $http, $window, $timeout, $interval, constants, config, captions, hotkeys, utils) {
    var accountChangeTimer = null;

    function init() {
      getCookies();
      initTabs();
      //declare scope variables used in view
      $scope.shortcutsInitiated = false;
      $scope.account = null;
      $scope.accountStatus = {
        state: null,
        btnColor: null,
        btnIcon: null
      };
      $scope.changeAccountStatus(constants.LP_CHAT_STATES.UNKNOWN);
      $scope.accountStatusUnkown = 0;
      $scope.accountStatusLoading = false;
      $scope.skill = null;
      $scope.operator = null;

      $scope.alertShown = false;
      $scope.domains = config.serverDomains;
      $scope.groupedFunctions = config.groupedFunctions;
      $scope.testAccounts = config.testAccounts;
      $scope.lpaAccounts = config.lpaAccounts;
      $scope.captions = captions;
      $scope.defaultServerName = '[' + _.find(config.serverDomains, function (domain) {
        return domain.url === $scope.cookies.defaultServerUrl
      }).name + '] - ' + $scope.cookies.defaultServerUrl;

      $scope.loaderHotNews = false;
      $scope.loaderAccountInfo = false;


      initShortcuts();
      resetVariables();
    }

    function groupFunctions(functionList) {
      console.log(functionList);
    }

    function initTabs() {
      $('.nav-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
      });
    }

    function resetIframeSrc() {
      $scope.iframeSrc = null;
    }

    function resetUnknownStatusCounter() {
      $scope.changeAccountStatus(constants.LP_CHAT_STATES.UNKNOWN);
      $scope.accountStatusUnkown = 0;
    }

    function resetAccountInformationTooltip() {
      $scope.accountInfo = {display: false, hotNews: null, salesforceInfo: null};
    }

    function resetVariables() {
      resetIframeSrc();
      resetUnknownStatusCounter();
      resetAccountInformationTooltip();
    }


    function getCookies() {
      $scope.cookies = cookieService.getCookies();
      $scope.livepersonUsername = $scope.cookies.livepersonUsername;
      $scope.livepersonPassword = $scope.cookies.livepersonPassword;
    }


    function getHotNews() {
      var def = $q.defer();
      $scope.loaderHotNews = true;
      var url = config.salesforceAPIDomain.hotNews;
      $rootScope.hotNews = [];

      $http.get(url + '' + $scope.account).
        success(function (data, status, headers, config) {
          def.resolve(data);
        }).
        error(function (data, status, headers, config) {
          $scope.showAlert('danger', 'Notice', captions.certificateError + "" + url);
          def.reject(captions.certificateError + "" + url);
        });

      return def.promise;
    }

    function getSalesforceInformation() {
      var def = $q.defer();
      $scope.loaderAccountInfo = true;
      var url = config.salesforceAPIDomain.accountInfo;

      $http.get(url + '' + $scope.account).
        success(function (data, status, headers, config) {
          def.resolve(data);
        }).
        error(function (data, status, headers, config) {
          $scope.showAlert('danger', 'Notice', captions.certificateError + "" + url);
          def.reject(captions.certificateError + "" + url);
        });

      return def.promise;
    }

    function getAccountInformation() {
      $q.all([getSalesforceInformation(), getHotNews()]).then(function (data) {
        $scope.accountInfo = {salesforceInfo: data[0], hotNews: data[1]}
        $scope.loaderHotNews = false;
        $scope.loaderAccountInfo = false;
      });
    }

    function getSiteVersion() {
      console.log($scope.cookies.defaultServerUrl, $scope.account);
      var url = utils.getSiteVersion($scope.cookies.defaultServerUrl, $scope.account);

      $scope.iframeSrc = url;

      // todo: This function doesn't work due to XSS problems
      // $timeout(function () {
      //  console.log(angular.element('#serverVersionIframe').contents());
      //}, 2000);

    }

    function checkAccountAvailability() {
      $scope.changeAccountStatus(constants.LP_CHAT_STATES.CHECKING);

      if ($scope.accountStatusUnkown >= config.maxUnknownRechecks) {
        $interval.cancel($scope.accountStatusChecker);
        $scope.changeAccountStatus(constants.LP_CHAT_STATES.UNKNOWN);
      }
      var lpServerName = config.defaultServerUrl;
      var lpNumber = $scope.account;
      var lpSkill = $scope.skill; //not yet in use
      var lpOperator = $scope.operator; //not yet in use
      var lpChannel = null;

      var skillParam = null;
      var operatorParam = null;
      var channelParam = null;

      if (typeof(lpSkill) == "undefined" || !lpSkill)
        skillParam = "";
      else
        skillParam = "&skill=" + encodeURI(lpSkill);
      if (typeof(lpOperator) == "undefined" || !lpOperator)
        operatorParam = "";
      else
        operatorParam = "&operator=" + encodeURI(lpOperator);
      if (typeof(lpChannel) == "undefined" || !lpChannel)
        channelParam = "";
      else
        channelParam = "&channel=" + encodeURI(lpChannel);

      var lpDOM = (document.getElementById) ? true : false;
      var lpIE = (document.all) ? true : false;
      var lpMAC_IE = ((navigator.platform) && (navigator.platform.toUpperCase().indexOf("MAC") >= 0) && (lpIE));
      var lpFindRepStateImage = new Image();

      if ((!lpIE) && (lpDOM)) {
        lpFindRepStateImage = document.createElement('IMG');
        lpFindRepStateImage.style.visibility = "hidden";
        document.body.appendChild(lpFindRepStateImage);
      } else if (lpMAC_IE) {
        document.writeln("<div style='visibility:hidden'><img src='blank' id='lpFindRepStateImage' name='lpFindRepStateImage'></div>");
        lpFindRepStateImage = document.lpFindRepstateImage;
      }

      lpFindRepStateImage.src = "https://" + lpServerName + "/hc/" + lpNumber + "/?cmd=repstate&site=" + lpNumber + "&useSize=true" + skillParam + operatorParam + channelParam + "&d=" + new Date().getTime();
      $timeout(function () {
        if (((lpDOM && !lpIE) || (lpMAC_IE)) && (!lpFindRepStateImage.complete)) {
          console.log(constants.LP_CHAT_STATES.UNKNOWN);
          $scope.accountStatusUnkown++;
        }
        var imageWidth = lpFindRepStateImage.width;
        if (imageWidth > 0) {
          if (imageWidth == 40) {
            $scope.accountStatus = constants.LP_CHAT_STATES.ONLINE;
          } else if (imageWidth == 50) {
            $scope.accountStatus = constants.LP_CHAT_STATES.OFFLINE;
          } else if (imageWidth == 60) {
            $scope.accountStatus = constants.LP_CHAT_STATES.OCCUPIED;
          } else {
            $scope.accountStatus = constants.LP_CHAT_STATES.UNKNOWN;
            $scope.accountStatusUnkown++;
          }
        }
        $scope.changeAccountStatus($scope.accountStatus);
      }, 1000);
    }

    function updateHistoryCookie() {
      $cookies.put("history", JSON.stringify($scope.cookies.history), {'expires': config.cookieExpirationDate});
    }

    function populateUrlWithParams(params, url) {
      var res = true;
      _.forEach(params, function (param) {
        var scopeParam = param.replace('$', '');
        res = res && $scope[scopeParam];
        if ($scope[scopeParam]) {
          url = url.replace(param, $scope[scopeParam]);
        }
      });

      if (!res) {
        return false;
      }
      return url;
    }

    function getMissingParams(params) {
      return _.filter(params, function (param) {
        return !$scope[param.replace('$', '')]
      });
    }

    function initShortcuts() {
      if (!$scope.shortcutsInitiated) {

        var map = _.map($scope.groupedFunctions, function (group) {
          return group.functions;
        });

        _.forEach(map, function (functions) {
          _.forEach(functions, function (func) {
            if (func.shortcut && func.url) {
              hotkeys.add({
                  combo: func.shortcut,
                  description: func.description,
                  allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                  callback: function () {
                    $scope.invokeFunction(func.url)
                  }
                }
              );
            }
          })
        });

        hotkeys.add({
          combo: 'esc',
          allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
          callback: function () {
            $scope.toggleAlert();
          }
        });
        $scope.shortcutsInitiated = true;
      }
    }

    $scope.$on('$destroy', function () {
      $interval.cancel($scope.accountStatusChecker);
    });


    $scope.$watch("account", function (newValue, oldValue) {

      function runFunctions() {
        resetVariables();
        getSiteVersion();
        getAccountInformation();
        checkAccountAvailability();
      }

      if (newValue !== oldValue) {
        if (accountChangeTimer) {
          $timeout.cancel(accountChangeTimer);
        }
        accountChangeTimer = $timeout(runFunctions, 500);
      }
    });


    $scope.toggleAlert = function () {
      $scope.alertShown = !$scope.alertShown;
    };

    $scope.checkAccountAvailability = function () {
      if ($scope.account !== null && $scope.account !== "") {
        checkAccountAvailability()
      }
    };

    $scope.changeAccountStatus = function (status) {
      switch (status) {
        case constants.LP_CHAT_STATES.UNKNOWN:
          $scope.accountStatus = {
            state: constants.LP_CHAT_STATES.UNKNOWN,
            btnColor: "btn-default",
            btnIcon: "fa-ban"
          };
          break;
        case constants.LP_CHAT_STATES.OFFLINE:
          $scope.accountStatus = {
            state: constants.LP_CHAT_STATES.OFFLINE,
            btnColor: "btn-danger",
            btnIcon: "fa-times-circle"
          };
          break;
        case constants.LP_CHAT_STATES.ONLINE:
          $scope.accountStatus = {
            state: constants.LP_CHAT_STATES.ONLINE,
            btnColor: "btn-success",
            btnIcon: "fa-comment-o"
          };
          break;
        case constants.LP_CHAT_STATES.OCCUPIED:
          $scope.accountStatus = {
            state: constants.LP_CHAT_STATES.OCCUPIED,
            btnColor: "btn-warning",
            btnIcon: "fa-clock-o"
          };
          break;
        case constants.LP_CHAT_STATES.CHECKING:
          $scope.accountStatus = {
            state: constants.LP_CHAT_STATES.CHECKING,
            btnColor: "btn-default",
            btnIcon: "fa-circle-o-notch fa-spin"
          };
          break;
      }
    };

    $scope.showAlert = function (notificationClass, notificationType, notificationMessage) {
      $scope.toggleAlert();
      $scope.notificationClass = notificationClass;
      $scope.notificationType = notificationType;
      $scope.notificationMessage = notificationMessage;
      $scope.alertShown = true;
      //$timeout($scope.hideAlert, 4000);
    };

    $scope.invokeFunction = function (url) {
      var variables = /\$(\w+)/g;
      var requiredParams = url.match(variables);
      var missingParameters = getMissingParams(requiredParams);

      if (missingParameters.length === 0) {
        $scope.addToAccountHistory({account: $scope.account, serverUrl: $scope.cookies.defaultServerUrl});
        var invokeUrl = null;
        if (url.indexOf('http://') === 0) {
          invokeUrl = populateUrlWithParams(requiredParams, url);
        } else {
          invokeUrl = 'https://' + $scope.cookies.defaultServerUrl + '' + populateUrlWithParams(requiredParams, url);
        }
        $window.open(invokeUrl, $scope.cookies.linkWindowTarget);
      } else {
        $scope.showAlert('danger', 'Alert', $scope.captions.missingParameters + ' Currently missing:  ' + missingParameters);
      }
    };

    $scope.setActiveDomain = function (url) {
      $scope.cookies.defaultServerUrl = url;
      $scope.defaultServerName = '[' + _.find(config.serverDomains, function (domain) {
        return domain.url === $scope.cookies.defaultServerUrl
      }).name + '] - ' + $scope.cookies.defaultServerUrl;
    };

    $scope.addToAccountHistory = function (item) {
      var exists = false;

      //Check to see if the account field is populated
      if (item.account) {
        //Is there already the same account in history?
        exists = _.find($scope.cookies.history, function (historyItem) {
          return historyItem.account.toLowerCase() === item.account.toLowerCase();
        });
      }
      if (!exists && item.account) {
        if ($scope.cookies.history.length === config.maxHistorySize) { //reached max length. Need to lose oldest history item
          $scope.cookies.history.pop();
          $scope.cookies.history.unshift({account: item.account, serverUrl: item.serverUrl, timeUsed: new Date()});
        }
        else { // push into history
          $scope.cookies.history.unshift({account: item.account, serverUrl: item.serverUrl, timeUsed: new Date()});
        }
        updateHistoryCookie();
      }
    };

    $scope.removeFromHistory = function (item) {
      $scope.cookies.history = _.filter($scope.cookies.history, function (value) {
        return value !== item;
      });
      updateHistoryCookie();
    };

    $scope.setAsAccount = function (item) {
      $scope.account = item.account;
      $scope.setActiveDomain(item.serverUrl);
    };

    $scope.toggleAdvancedTools = function () {
      $scope.showAdvancedTools === 'true' ? $scope.showAdvancedTools = 'false' : $scope.showAdvancedTools = 'true';
    };

    $scope.toggleCheatSheet = function () {
      hotkeys.toggleCheatSheet();
    };

    init();

  });



