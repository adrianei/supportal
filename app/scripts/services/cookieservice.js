'use strict';

/**
 * @ngdoc service
 * @name supportalApp.cookieService
 * @description
 * # cookieService
 * Service in the supportalApp.
 */
angular.module('supportalApp')
  .service('cookieService', function ($cookies, config) {
    this.getCookies = function () {
      var cookies = {
        personalFullName: $cookies.get("personalFullName") || null,
        personalEmailAddress: $cookies.get("personalEmailAddress") || null,
        livepersonUsername: $cookies.get("livepersonUsername") || null,
        livepersonPassword: $cookies.get("livepersonPassword") || null,
        //salesforceUsername: $cookies.get("salesforceUsername") || null,
        //salesforcePassword: $cookies.get("salesforcePassword") || null,
        //exactTargetUsername: $cookies.get("exactTargetUsername") || null,
        //exactTargetPassword: $cookies.get("exactTargetPassword") || null,
        linkWindowTarget: $cookies.get("linkWindowTarget") || '_blank',
        showAdvancedTools: $cookies.get("showAdvancedTools") || false,
        defaultServerUrl: $cookies.get("defaultServerUrl") || config.serverDomains[0].url,
        userAvatar: $cookies.get("userAvatar") || 'images/avatars/' + config.avatars[0],
        history: angular.fromJson($cookies.get("history")) || [],
        favorites: angular.fromJson($cookies.get("favorites")) || config.favorites
      };

      if (!$cookies.get("favorites")) {
        cookies.favorites = [];
      }
      else {
        cookies.favorites = JSON.parse($cookies.get("favorites"));
      }


      return cookies;
    }
  });
